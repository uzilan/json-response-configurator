# JSON Response Configurator #
The JSON Response Configurator (JSC) is a way to control the results of a REST query to a Java backend that uses RESTEasy and Jackson.

See the [wiki](https://bitbucket.org/fferm/json-response-configurator/wiki/Home) for more information