package se.fermitet.jrc.games.domain;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class ChangeTest {
	@Test
	public void _hasValidConstructor() throws Exception {
		assertThat(Change.class, hasValidBeanConstructor());
	}
	
	@Test
	public void _hasValidGettersAndSetters() throws Exception {
		assertThat(Change.class, hasValidGettersAndSetters());
	}

}
