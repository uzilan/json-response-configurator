package se.fermitet.jrc.webshop.domain;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import se.fermitet.jrc.webshop.domain.OrderLine;

public class OrderLineTest {
	@Test
	public void orderLine_hasValidConstructor() throws Exception {
		assertThat(OrderLine.class, hasValidBeanConstructor());
	}
	
	@Test
	public void orderLine_hasValidGettersAndSetters() throws Exception {
		assertThat(OrderLine.class, hasValidGettersAndSetters());
	}
	
	@Test
	public void orderLine_hasValidToString() throws Exception {
		assertThat(OrderLine.class, hasValidBeanToString());
	}

}
