package se.fermitet.jrc.webshop.domain;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import se.fermitet.jrc.webshop.domain.Customer;

public class CustomerTest {
	@Test
	public void customer_hasValidConstructor() throws Exception {
		assertThat(Customer.class, hasValidBeanConstructor());
	}
	
	@Test
	public void customer_hasValidGettersAndSetters() throws Exception {
		assertThat(Customer.class, hasValidGettersAndSetters());
	}
	
	@Test
	public void customer_hasValidToString() throws Exception {
		assertThat(Customer.class, hasValidBeanToStringExcluding("orders"));
	}

}
