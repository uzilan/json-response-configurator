package se.fermitet.jrc.games.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import se.fermitet.jrc.games.data.GamesDataGenerator;
import se.fermitet.jrc.games.domain.Game;

@Path("/")
public class GamesRest {
	private GamesDataGenerator gamesDataGenerator;

	public GamesRest() {
		super();
		this.gamesDataGenerator = new GamesDataGenerator();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/game")
	public Response getSingleGame() {
		Game result = gamesDataGenerator.getSingleGame(); 
		return Response.ok().entity(result).build();
	}

}
