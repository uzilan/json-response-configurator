package se.fermitet.jrc.games.domain;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;

@Entity
public class Team {
	@Attribute
	private String name;
	
	@Attribute
	private String hometown;

	public Team() {
		super();
	}

	public Team(String name, String hometown) {
		super();
		this.name = name;
		this.hometown = hometown;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHometown() {
		return hometown;
	}

	public void setHometown(String hometown) {
		this.hometown = hometown;
	}

	

}
