package se.fermitet.jrc.games.domain;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Relation;

public class Change {
	@Attribute
	private int minute;
	
	@Relation 
	private Player playerIn;
	
	@Relation 
	private Player playerOut;
	
	@Relation
	private Team team;

	public Change() {
		super();
	}

	public Change(Team team, int minute, Player playerIn, Player playerOut) {
		super();
		this.minute = minute;
		this.playerIn = playerIn;
		this.playerOut = playerOut;
		this.team = team;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public Player getPlayerIn() {
		return playerIn;
	}

	public void setPlayerIn(Player playerIn) {
		this.playerIn = playerIn;
	}

	public Player getPlayerOut() {
		return playerOut;
	}

	public void setPlayerOut(Player playerOut) {
		this.playerOut = playerOut;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	
}
