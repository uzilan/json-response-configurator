package se.fermitet.jrc.games.data;

import java.time.LocalDateTime;

import se.fermitet.jrc.games.domain.Arena;
import se.fermitet.jrc.games.domain.Card;
import se.fermitet.jrc.games.domain.Card.Type;
import se.fermitet.jrc.games.domain.Change;
import se.fermitet.jrc.games.domain.Game;
import se.fermitet.jrc.games.domain.Goal;
import se.fermitet.jrc.games.domain.Player;
import se.fermitet.jrc.games.domain.Team;

public class GamesDataGenerator {
	public Game getSingleGame() {
		Team sundsvall = new Team("GIF Sundsvall", "Sundsvall");
		Team hammarby = new Team("Hammarby", "Stockholm");
		
		Arena arena = new Arena("Norrporten arena", "Sundsvall", 8500);
		
		Player moros = new Player("Carlos Moros Garcia", 16);
		Player sundberg = new Player("Noah Sonko Sundberg", 19);
		Player naurin = new Player("Tommy Naurin", 17);
		Player rajalasko = new Player("Sebastian Rajalasko", 11);
		Player myrestam = new Player("David Myrestam", 4);
		Player pirttijoki = new Player("Juho Pirttijoki", 2);
		Player sigurdsson = new Player("Kristinn Freyr Sigurdssson", 6);
		Player morsay = new Player("Jonathan Morsay", 32);
		
		Player svendsen = new Player("Sander Svendsen", 21);
		Player paulsen = new Player("Björn Paulsen", 4);
		Player smarason = new Player("Arnor Smarason", 11);
		Player saevarsson = new Player("Birkir Már Sævarsson", 2);
		Player bakir = new Player("Kennedy Bakircioglu", 10);
		Player andersen = new Player("Jeppe Andersen", 8);
		Player tankovic = new Player("Muamer Tankovic", 22);
		Player bengtsson = new Player("Leo Bengtsson", 34);
		
		Game game = new Game(sundsvall, hammarby, LocalDateTime.of(2017,10,29,15,0,0), 5157, arena);
		
		game.addGoal(new Goal(sundsvall, 17, moros, sundberg));
		game.addGoal(new Goal(hammarby, 42, svendsen, saevarsson));
		game.addGoal(new Goal(hammarby, 52, svendsen, bakir));
		game.addGoal(new Goal(hammarby, 68, paulsen, null));
		game.addGoal(new Goal(hammarby, 91, smarason, svendsen));
		
		game.addCard(new Card(hammarby, 44, Type.YELLOW, andersen));
		game.addCard(new Card(sundsvall, 75, Type.YELLOW, naurin));
		
		game.addChange(new Change(sundsvall, 65, rajalasko, myrestam));
		game.addChange(new Change(sundsvall, 73, pirttijoki, moros));
		game.addChange(new Change(sundsvall, 78, sigurdsson, morsay));
		game.addChange(new Change(hammarby, 82, bengtsson, tankovic));
		
		return game;
	}
}
