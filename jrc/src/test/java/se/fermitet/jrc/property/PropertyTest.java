package se.fermitet.jrc.property;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import java.util.Collection;
import java.util.stream.Collectors;

import org.junit.Test;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;

public class PropertyTest {


	@Test
	public void getAttribute() throws Exception {
		TestClass testObject = new TestClass();

		testNonCompoundProperty(testObject, "getAttribute", true, testObject.getGetAttribute(), true);
	}

	@Test
	public void isAttribute() throws Exception {
		TestClass testObject = new TestClass();

		testNonCompoundProperty(testObject, "isAttribute", true, testObject.isIsAttribute(), true);
	}

	@Test
	public void hasAttribute() throws Exception {
		TestClass testObject = new TestClass();

		testNonCompoundProperty(testObject, "hasAttribute", true, testObject.hasHasAttribute(), true);
	}

	@Test
	public void relation() throws Exception {
		TestClass testObject = new TestClass();

		testNonCompoundProperty(testObject, "relation", false, testObject.getRelation(), true);
	}

	@Test
	public void superclass() throws Exception {
		TestSubclass testObject = new TestSubclass();
		testNonCompoundProperty(testObject, "getAttribute", true, testObject.getGetAttribute(), true);
	}

	@Test
	public void attributeOnGetter() throws Exception {
		TestClass testObject = new TestClass();

		testNonCompoundProperty(testObject, "attributeOnGetter", true, testObject.getAttributeOnGetter(), false);
	}

	private void testNonCompoundProperty(TestClass testObject, String propName, boolean isAttribute, String expectedValue, boolean testSetter) throws Exception {
		Property property = null;
		if (isAttribute)
			property = new AttributeProperty(propName, testObject.getClass());
		else
			property = new RelationProperty(propName, testObject.getClass());

		assertThat(property.getName(), is(propName));
		assertThat(property.getValue(testObject), is(expectedValue));
		assertFalse(property instanceof CompoundProperty);
		
		if(! testSetter) return;
		String newValue = "NEW";
		property.setValue(newValue, testObject);
		assertThat(property.getValue(testObject), is(newValue));
	}

	@Test(expected = IllegalArgumentException.class)
	public void illegalName() throws Exception {
		new AttributeProperty("ILLEGAL NAME", TestClass.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void nonAttributeOrRelation() throws Exception {
		new RelationProperty("nonAttributeOrRelation", TestClass.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void attributeOnRelation() throws Exception {
		new AttributeProperty("relation", TestClass.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void relationOnattribute() throws Exception {
		new RelationProperty("getAttribute", TestClass.class);
	}

	@Test
	public void defaultProperties() throws Exception {
		Collection<Property> defaults = Property.getDefaultProperties(TestClass.class);

		assertThat(defaults.size(), is(4));

		Collection<String> propertyNames = defaults.stream().map(p -> p.getName()).collect(Collectors.toList());

		assertTrue(propertyNames.contains("getAttribute"));
		assertTrue(propertyNames.contains("isAttribute"));
		assertTrue(propertyNames.contains("hasAttribute"));
		assertTrue(propertyNames.contains("attributeOnGetter"));
	}

	@SuppressWarnings("unused")
	@Test(expected = IllegalArgumentException.class)
	public void callingWrongClass_get() throws Exception {
		TestClass testData = new TestClass();
		Property prop = new AttributeProperty("getAttribute", testData.getClass());

		Object value = prop.getValue(new Integer(23));	// Should be a string to be correct
	}

	@Test(expected = IllegalArgumentException.class)
	public void callingWrongClass_set() throws Exception {
		TestClass testData = new TestClass();
		Property prop = new AttributeProperty("getAttribute", testData.getClass());

		prop.setValue("NEW", new Integer(23));	// Should be a string to be correct
	}

	@Test
	public void getPropertyByName() throws Exception {
		Property result = Property.getByName("getAttribute", TestClass.class);
		assertThat(result.getName(), is("getAttribute"));
		assertTrue(result instanceof AttributeProperty);

		result = Property.getByName("relation", TestClass.class);
		assertThat(result.getName(), is("relation"));
		assertTrue(result instanceof RelationProperty);

		result = Property.getByName("attributeLink.getAttribute", TestSourceClass.class);
		assertThat(result.getName(), is("attributeLink.getAttribute"));
		assertTrue(result instanceof CompoundProperty);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getPropertyByName_wrongName() throws Exception {
		Property.getByName("ILLEGAL NAME", TestClass.class);
	}


	@Entity
	static class TestClass {
		@Attribute private String getAttribute = "getAttribute";
		@Attribute private String isAttribute = "isAttribute";
		@Attribute private String hasAttribute = "hasAttribute";
		@SuppressWarnings("unused")
		private String nonAttributeOrRelation;

		@Relation private String relation = "relation";

		public String getGetAttribute() {
			return getAttribute;
		}
		
		public void setGetAttribute(String getAttribute) {
			this.getAttribute = getAttribute;
		}

		public String isIsAttribute() {
			return isAttribute;
		}
		
		public String getIsAttribute() {
			return isAttribute;
		}

		public void setIsAttribute(String isAttribute) {
			this.isAttribute = isAttribute;
		}

		public String hasHasAttribute() {
			return hasAttribute;
		}
		
		public String getHasAttribute() {
			return hasAttribute;
		}

		public void setHasAttribute(String hasAttribute) {
			this.hasAttribute = hasAttribute;
		}

		@Attribute
		public String getAttributeOnGetter() {
			return "attributeOnGetter";
		}
		

		public String getRelation() {
			return relation;
		}

		public void setRelation(String relation) {
			this.relation = relation;
		}
		
	}

	class TestSubclass extends TestClass {}

	static class TestSourceClass {
		@Attribute private TestClass attributeLink = new TestClass();
		public TestClass getAttributeLink() {
			return attributeLink;
		}
	}

	static class TestDoubleSourceClass {
		@Attribute private TestSourceClass doubleLink = new TestSourceClass();
		public TestSourceClass getDoubleLink() {
			return doubleLink;
		}
	}
}
