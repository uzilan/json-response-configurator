package se.fermitet.jrc.restparameter;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import org.junit.Test;

import se.fermitet.jrc.restparameter.SortRestParameter;
import se.fermitet.jrc.restparameter.SortRestParameter.SortRestParameterKeyword;

public class SortRestParameterTest {
	@Test
	public void ascendingWithName() throws Exception {
		SortRestParameter param = new SortRestParameter("name");
		assertThat(param.getSortByName(), is("name"));
		assertNull(param.getSortByKeyword());
		assertThat(param.isAscending(), is(true));
		assertThat(param.isDescending(), is(false));
		assertNull(param.getSortCollection());
	}

	@Test
	public void descendingWithName() throws Exception {
		SortRestParameter param = new SortRestParameter("-name");
		assertThat(param.getSortByName(), is("name"));
		assertNull(param.getSortByKeyword());
		assertThat(param.isAscending(), is(false));
		assertThat(param.isDescending(), is(true));
		assertNull(param.getSortCollection());
	}

	@Test
	public void ascendingWithKeyword() throws Exception {
		SortRestParameter param = new SortRestParameter("MAPKEY");
		assertNull(param.getSortByName());
		assertThat(param.getSortByKeyword(), is(SortRestParameterKeyword.MAPKEY));
		assertThat(param.isAscending(), is(true));
		assertThat(param.isDescending(), is(false));
		assertNull(param.getSortCollection());
	}

	@Test
	public void descendingWithKeyword() throws Exception {
		SortRestParameter param = new SortRestParameter("-MAPKEY");
		assertNull(param.getSortByName());
		assertThat(param.getSortByKeyword(), is(SortRestParameterKeyword.MAPKEY));
		assertThat(param.isAscending(), is(false));
		assertThat(param.isDescending(), is(true));
		assertNull(param.getSortCollection());
	}

	@Test
	public void withCollection() throws Exception {
		SortRestParameter param = new SortRestParameter("collection", "name");
		assertThat(param.getSortByName(), is("name"));
		assertNull(param.getSortByKeyword());
		assertThat(param.isAscending(), is(true));
		assertThat(param.isDescending(), is(false));

		assertThat(param.getSortCollection(), is("collection"));
	}

	@Test
	public void relatedParameter() throws Exception {
		SortRestParameter orig = new SortRestParameter("collection", "name");
		SortRestParameter related = orig.getRelatedParamter();

		assertThat(related.getSortByName(), is("name"));
		assertNull(related.getSortByKeyword());
		assertThat(related.isAscending(), is(true));
		assertNull(related.getSortCollection());
	}

	@Test
	public void relatedParameter_onRegular() throws Exception {
		SortRestParameter orig = new SortRestParameter("name");
		assertNull(orig.getRelatedParamter());
	}

	@Test
	public void relatedParameter_double() throws Exception {
		SortRestParameter orig = new SortRestParameter("collection1.collection2", "-MAPKEY");
		SortRestParameter related = orig.getRelatedParamter();

		assertNull(related.getSortByName());
		assertThat(related.getSortByKeyword(), is(SortRestParameterKeyword.MAPKEY));
		assertThat(related.isAscending(), is(false));
		assertThat(related.getSortCollection(), is("collection2"));
	}

}
