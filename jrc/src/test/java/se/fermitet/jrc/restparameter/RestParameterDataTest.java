package se.fermitet.jrc.restparameter;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import se.fermitet.jrc.restparameter.RestParameterData;

public class RestParameterDataTest {
	@Test
	public void RestParameterData_hasValidConstructor() {
		assertThat(RestParameterData.class, hasValidBeanConstructor());
	}

	@Test
	public void RestParameterData_hasValidGetterAndSetters() throws Exception {
		assertThat(RestParameterData.class, hasValidGettersAndSetters());
	}
}
