package se.fermitet.jrc.restparameter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import se.fermitet.jrc.restparameter.RestParameterData;
import se.fermitet.jrc.restparameter.RestParameterFilter;
import se.fermitet.jrc.restparameter.SortRestParameter.SortRestParameterKeyword;

@RunWith(MockitoJUnitRunner.class)
public class RestParameterFilterTest {
	@Mock
	private RestParameterData attributeAndRelationData;

	@Mock
	private UriInfo uriInfo;

	@Mock
	private ContainerRequestContext containerRequestContext;

	@InjectMocks
	private RestParameterFilter filter;

	private MultivaluedMap<String, String> returnMap;

	@Before
	public void setUp() throws Exception {
		filter = new RestParameterFilter();
		filter.restParameterData = new RestParameterData();

		when(containerRequestContext.getUriInfo()).thenReturn(uriInfo);

		returnMap = new MultivaluedHashMap<String, String>();
		when(uriInfo.getQueryParameters()).thenReturn(returnMap);
	}

	@Test
	public void empty() throws Exception {
		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));
		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
		assertThat(filter.restParameterData.getSortParameters().size(), is(0));
	}

	@Test
	public void singleAtt() throws Exception {
		returnMap.put("att", Arrays.asList("value"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(1));
		assertThat(filter.restParameterData.getAttributeNames().contains("value"), is(true));

		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
		assertThat(filter.restParameterData.getSortParameters().size(), is(0));
	}

	@Test
	public void attWithComma() throws Exception {
		returnMap.put("att", Arrays.asList("value,nextValue,thirdValue"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(3));
		assertThat(filter.restParameterData.getAttributeNames().contains("value"), is(true));
		assertThat(filter.restParameterData.getAttributeNames().contains("nextValue"), is(true));
		assertThat(filter.restParameterData.getAttributeNames().contains("thirdValue"), is(true));

		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
		assertThat(filter.restParameterData.getSortParameters().size(), is(0));
	}

	@Test
	public void multipleAtt() throws Exception {
		returnMap.put("att", Arrays.asList("value,nextValue","thirdValue"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(3));
		assertThat(filter.restParameterData.getAttributeNames().contains("value"), is(true));
		assertThat(filter.restParameterData.getAttributeNames().contains("nextValue"), is(true));
		assertThat(filter.restParameterData.getAttributeNames().contains("thirdValue"), is(true));

		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
		assertThat(filter.restParameterData.getSortParameters().size(), is(0));
	}

	@Test
	public void singleRel() throws Exception {
		returnMap.put("rel", Arrays.asList("value"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));

		assertThat(filter.restParameterData.getRelationNames().size(), is(1));
		assertThat(filter.restParameterData.getRelationNames().contains("value"), is(true));

		assertThat(filter.restParameterData.getSortParameters().size(), is(0));
	}

	@Test
	public void relWithComma() throws Exception {
		returnMap.put("rel", Arrays.asList("value,nextValue,thirdValue"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));

		assertThat(filter.restParameterData.getRelationNames().size(), is(3));
		assertThat(filter.restParameterData.getRelationNames().contains("value"), is(true));
		assertThat(filter.restParameterData.getRelationNames().contains("nextValue"), is(true));
		assertThat(filter.restParameterData.getRelationNames().contains("thirdValue"), is(true));

		assertThat(filter.restParameterData.getSortParameters().size(), is(0));
	}

	@Test
	public void multipleRel() throws Exception {
		returnMap.put("rel", Arrays.asList("value,nextValue","thirdValue"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));

		assertThat(filter.restParameterData.getRelationNames().size(), is(3));
		assertThat(filter.restParameterData.getRelationNames().contains("value"), is(true));
		assertThat(filter.restParameterData.getRelationNames().contains("nextValue"), is(true));
		assertThat(filter.restParameterData.getRelationNames().contains("thirdValue"), is(true));

		assertThat(filter.restParameterData.getSortParameters().size(), is(0));
	}

	@Test
	public void singleSort_name() throws Exception {
		returnMap.put("sort", Arrays.asList("value"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));
		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
		assertThat(filter.restParameterData.getSortParameters().size(), is(1));

		SortRestParameter param = filter.restParameterData.getSortParameters().iterator().next();
		assertThat(param.getSortByName(), is("value"));
		assertThat(param.isAscending(), is(true));
	}

	@Test
	public void singleSort_name_descending() throws Exception {
		returnMap.put("sort", Arrays.asList("-value"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));
		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
		assertThat(filter.restParameterData.getSortParameters().size(), is(1));

		SortRestParameter param = filter.restParameterData.getSortParameters().iterator().next();
		assertThat(param.getSortByName(), is("value"));
		assertThat(param.isDescending(), is(true));
	}

	@Test
	public void singleSort_keyword() throws Exception {
		returnMap.put("sort", Arrays.asList("MAPKEY"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));
		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
		assertThat(filter.restParameterData.getSortParameters().size(), is(1));

		SortRestParameter param = filter.restParameterData.getSortParameters().iterator().next();
		assertThat(param.getSortByKeyword(), is(SortRestParameterKeyword.MAPKEY));
		assertThat(param.isAscending(), is(true));
	}

	@Test
	public void singleSort_keyword_descending() throws Exception {
		returnMap.put("sort", Arrays.asList("-MAPKEY"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));
		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
		assertThat(filter.restParameterData.getSortParameters().size(), is(1));

		SortRestParameter param = filter.restParameterData.getSortParameters().iterator().next();
		assertThat(param.getSortByKeyword(), is(SortRestParameterKeyword.MAPKEY));
		assertThat(param.isDescending(), is(true));
	}

	@Test
	public void sortWithComma() throws Exception {
		returnMap.put("sort", Arrays.asList("value1,value2"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));
		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
		assertThat(filter.restParameterData.getSortParameters().size(), is(2));

		Iterator<SortRestParameter> iter = filter.restParameterData.getSortParameters().iterator();
		SortRestParameter param1 = iter.next();
		assertThat(param1.getSortByName(), is("value1"));
		assertThat(param1.isAscending(), is(true));

		SortRestParameter param2 = iter.next();
		assertThat(param2.getSortByName(), is("value2"));
		assertThat(param2.isAscending(), is(true));
	}

	@Test
	public void multipleSort() throws Exception {
		returnMap.put("sort", Arrays.asList("value1", "value2"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));
		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
		assertThat(filter.restParameterData.getSortParameters().size(), is(2));

		Iterator<SortRestParameter> iter = filter.restParameterData.getSortParameters().iterator();
		SortRestParameter param1 = iter.next();
		assertThat(param1.getSortByName(), is("value1"));
		assertThat(param1.isAscending(), is(true));

		SortRestParameter param2 = iter.next();
		assertThat(param2.getSortByName(), is("value2"));
		assertThat(param2.isAscending(), is(true));
	}

	@Test
	public void attAndRelAndSort() throws Exception {
		returnMap.put("att", Arrays.asList("attValue"));
		returnMap.put("rel", Arrays.asList("relValue"));
		returnMap.put("sort", Arrays.asList("sortValue"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(1));
		assertThat(filter.restParameterData.getAttributeNames().contains("attValue"), is(true));

		assertThat(filter.restParameterData.getRelationNames().size(), is(1));
		assertThat(filter.restParameterData.getRelationNames().contains("relValue"), is(true));

		assertThat(filter.restParameterData.getSortParameters().size(), is(1));
		assertThat(filter.restParameterData.getSortParameters().iterator().next().getSortByName(), is("sortValue"));
	}

	@Test
	public void otherParameters() throws Exception {
		returnMap.put("other", Arrays.asList("value,nextValue,thirdValue"));

		filter.filter(containerRequestContext);

		assertThat(filter.restParameterData.getAttributeNames().size(), is(0));
		assertThat(filter.restParameterData.getRelationNames().size(), is(0));
	}

	@Test
	public void sortCollectionParameters() throws Exception {
		returnMap.put("sort.array", Arrays.asList("attValue"));

		filter.filter(containerRequestContext);

		List<SortRestParameter> sortParameters = filter.restParameterData.getSortParameters();

		assertThat(sortParameters.size(), is(1));

		SortRestParameter sortParameter = sortParameters.iterator().next();

		assertThat(sortParameter.getSortByName(), is("attValue"));
		assertThat(sortParameter.getSortCollection(), is("array"));
	}


}
