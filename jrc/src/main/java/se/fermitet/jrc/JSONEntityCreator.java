package se.fermitet.jrc;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.ejb.Stateless;

import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.property.AttributeProperty;
import se.fermitet.jrc.property.CompoundProperty;
import se.fermitet.jrc.property.Property;
import se.fermitet.jrc.property.RelationProperty;
import se.fermitet.jrc.restparameter.RestParameterData;
import se.fermitet.jrc.restparameter.SortRestParameter;
import se.fermitet.jrc.restparameter.SortRestParameter.SortRestParameterKeyword;

@Stateless
public class JSONEntityCreator {

    /**
     * @throws IllegalArgumentException if there is a problem with the attributes or relations of if the getter throws an exception
     * @return
     */
	public Object createJson(Object input, RestParameterData restParameters) {
		Object inputObjectWhichIsNotMapOrCollection = getInputObjectWhichIsNotMapOrCollection(input);
		
		if (inputObjectWhichIsNotMapOrCollection == null) return null;
		
		Collection<Property> properties = getPropertiesFromNames(restParameters.getAttributeNames(), restParameters.getRelationNames(), inputObjectWhichIsNotMapOrCollection.getClass());
		
		return createJsonByProperties(input, properties, restParameters.getSortParameters());
	}
	
    @SuppressWarnings("unchecked")
    private Object createJsonByProperties(Object input, Collection<Property> properties, List<SortRestParameter> sortParameters) {
        try {
            if (input instanceof Collection<?>) {
                return createJsonFromCollection((Collection<Object>) input, properties, sortParameters);
            } else if (input instanceof Map<?, ?>) {
                return createJsonFromMap((Map<Object, Object>) input, properties, sortParameters);
            } else {
                return createJsonFromObject(input, properties, sortParameters);
            }

        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException("Exception when creating JSON Entity {}", e);
        }
    }

    private Object createJsonFromObject(Object input, Collection<Property> properties, List<SortRestParameter> sortParameters) throws IllegalAccessException, InvocationTargetException {
        if (input == null) return null;
        if (isEntity(input.getClass())) {
            return createJsonFromEntity(input, properties, sortParameters);
        } else {
            return input;
        }
    }

    private Map<String, Object> createJsonFromEntity(Object input, Collection<Property> properties, List<SortRestParameter> sortParameters) throws IllegalAccessException, InvocationTargetException {
        Map<String, Object> map = new HashMap<String, Object>();

        properties = addDefaultPropertiesIfNeeded(properties, input);
        for (Property property : properties) {
            if (property instanceof CompoundProperty)
                continue;

            Object value = property.getValue(input);

            Collection<Property> newProperties = stripProperties(properties, property.getName(), value);
            List<SortRestParameter> newSortParameters = stripSortParameters(sortParameters, property.getName());

            Object jsonObject = createJsonByProperties(value, newProperties, newSortParameters);
            map.put(property.getName(), jsonObject);
        }
        return map;
    }

    private Collection<Property> addDefaultPropertiesIfNeeded(Collection<Property> properties, Object input) {
        boolean hasAttributeProperties = properties.stream()
                .filter(p -> (p instanceof AttributeProperty)).findFirst().isPresent();

        if (!hasAttributeProperties) {
            properties.addAll(Property.getDefaultProperties(input.getClass()));
        }

        return properties;
    }

    private List<?> createJsonFromCollection(Collection<Object> inputCollection, Collection<Property> properties, List<SortRestParameter> sortParameters) throws IllegalAccessException, InvocationTargetException {
        List<Object> ret = new ArrayList<Object>();

        for (Object inputObject : sortInputCollection(inputCollection, sortParameters)) {
            ret.add(createJsonByProperties(inputObject, properties, null));
        }

        return ret;
    }

    private Map<?, ?> createJsonFromMap(Map<Object, Object> input, Collection<Property> properties, List<SortRestParameter> sortParameters) throws IllegalAccessException, InvocationTargetException {
        Map<Object, Object> ret = new LinkedHashMap<Object, Object>();
        for (Object key : sortInputMap(input, sortParameters).keySet()) {
            ret.put(key, createJsonByProperties(input.get(key), properties, null));
        }

        return ret;
    }

    private Collection<Object> sortInputCollection(Collection<Object> inputCollection, List<SortRestParameter> sortParameters) {
        if (sortParameters == null || sortParameters.isEmpty())
            return inputCollection;

        List<Object> sortedInput = new ArrayList<Object>(inputCollection);

        Class<? extends Object> classOfInputBean = getInputObjectWhichIsNotMapOrCollection(inputCollection).getClass();

        for (SortRestParameter sortParameter : sortParameters) {
            if (sortParameter.getSortByKeyword() == SortRestParameterKeyword.MAPKEY) {
            	throw new IllegalArgumentException("Trying to sort a collection with a sort parameter which is not legal for collections. Parameter: " + sortParameter);
            }
        }

        sortedInput.sort((Object o1, Object o2) -> {
            return getPropertyValueComparatorResult(sortParameters, classOfInputBean, o1, o2);
        });

        return sortedInput;
    }

    private Map<Object, Object> sortInputMap(Map<Object, Object> inputMap, List<SortRestParameter> sortParameters) {
        if (sortParameters == null || sortParameters.isEmpty())
            return inputMap;

        SortRestParameter firstSortParameter = sortParameters.iterator().next();
        if (firstSortParameter.getSortCollection() != null) return inputMap;

        SortRestParameterKeyword keyword = firstSortParameter.getSortByKeyword();

        if (keyword != null && keyword.equals(SortRestParameterKeyword.MAPKEY)) {
            return sortInputMapBasedOnKey(inputMap, firstSortParameter);
        } else {
            return sortInputMapBasedOnAttributes(inputMap, sortParameters);
        }
    }

    @SuppressWarnings("unchecked")
    private Map<Object, Object> sortInputMapBasedOnKey(Map<Object, Object> inputMap, SortRestParameter sortParameter) {
        Map<Object, Object> ret = new TreeMap<Object, Object>(new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                Comparable<Object> c1 = (Comparable<Object>) o1;
                Comparable<Object> c2 = (Comparable<Object>) o2;

                int directionMultiplier = sortParameter.isAscending() ? 1 : -1;
                return c1.compareTo(c2) * directionMultiplier;
            }
        });

        ret.putAll(inputMap);

        return ret;
    }

    @SuppressWarnings("unchecked")
    private Map<Object, Object> sortInputMapBasedOnAttributes(Map<Object, Object> inputMap, List<SortRestParameter> sortParameters) {
        Class<? extends Object> classOfInputBean = getInputObjectWhichIsNotMapOrCollection(inputMap).getClass();

        List<Map.Entry<Object, Object>> listOfEntries = new ArrayList<>(inputMap.entrySet());
        listOfEntries.sort((Object o1, Object o2) -> {
            Entry<Object, Object> m1 = (Map.Entry<Object, Object>) o1;
            Entry<Object, Object> m2 = (Map.Entry<Object, Object>) o2;

            return getPropertyValueComparatorResult(sortParameters, classOfInputBean, m1.getValue(), m2.getValue());
        });

        Map<Object, Object> result = new LinkedHashMap<>();
        for (Entry<Object, Object> entry : listOfEntries) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    private int getPropertyValueComparatorResult(List<SortRestParameter> sortParameters, Class<? extends Object> classOfInputBean, Object v1, Object v2) {
        for (SortRestParameter sortParameter : sortParameters) {
            if (sortParameter.getSortCollection() != null) continue;

            Property prop = Property.getByName(sortParameter.getSortByName(), classOfInputBean);

            Comparable<Object> value1 = (Comparable<Object>) prop.getValue(v1);
            Comparable<Object> value2 = (Comparable<Object>) prop.getValue(v2);

            int directionMultiplier = sortParameter.isAscending() ? 1 : -1;
            int result = value1.compareTo(value2) * directionMultiplier;
            if (result != 0)
                return result;
        }
        return 0;
    }

    private Collection<Property> getPropertiesFromNames(Collection<String> attributeNames, Collection<String> relationNames, Class<?> classOfInput) {
        Collection<Property> ret = new ArrayList<Property>();

        if (attributeNames != null) {
            for (String name : attributeNames) {
                if (Property.isNameCompound(name)) {
                    ret.add(new CompoundProperty(name, classOfInput));
                } else {
                    ret.add(new AttributeProperty(name, classOfInput));
                }
            }
        }

        if (relationNames != null) {
            for (String name : relationNames) {
                if (Property.isNameCompound(name)) {
                    ret.add(new CompoundProperty(name, classOfInput));
                } else {
                    ret.add(new RelationProperty(name, classOfInput));
                }
            }
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    private Object getInputObjectWhichIsNotMapOrCollection(Object input) {
        if (input == null) return null;

        Object aValue = null;

        if (input instanceof Map<?, ?>) {
            Map<Object, Object> mInput = (Map<Object, Object>) input;
            if (mInput.isEmpty()) return null;

            Object aKey = mInput.keySet().iterator().next();
            aValue = mInput.get(aKey);
        } else if(input instanceof Collection<?>) {
            Collection<Object> cInput = (Collection<Object>) input;
            if (cInput.isEmpty()) return null;

            aValue = cInput.iterator().next();
        } else {
            aValue = input;
        }

        if ((aValue instanceof Map<?, ?>) || (aValue instanceof Collection<?>))
            aValue = getInputObjectWhichIsNotMapOrCollection(aValue);

        return aValue;
    }

    private boolean isEntity(Class<?> clz) {
        return clz.isAnnotationPresent(Entity.class);
    }

    private Collection<Property> stripProperties(Collection<Property> propertiesBeforeStrip, String fieldName, Object relatedObject) {
        return propertiesBeforeStrip.stream()
                .filter(p -> p instanceof CompoundProperty)
                .filter(p -> ((CompoundProperty) p).getMovementName().equals(fieldName))
                .map(p -> {
                    CompoundProperty c = (CompoundProperty) p;
                    Object relatedObjectNotMapOrCollection = getInputObjectWhichIsNotMapOrCollection(relatedObject);
                    if (relatedObjectNotMapOrCollection != null)
                        return Property.getByName(c.getRelatedName(), relatedObjectNotMapOrCollection.getClass());
                    else
                        return null;
                    })
                .collect(Collectors.toList());
    }

    private List<SortRestParameter> stripSortParameters(List<SortRestParameter> parametersBeforeStrip, String fieldName) {
        if (parametersBeforeStrip == null || parametersBeforeStrip.isEmpty()) return null;
        return parametersBeforeStrip.stream()
                .filter(p -> p.getRelatedParamter() != null)
                .filter(p -> p.getSortCollection().equals(fieldName))
                .map(p -> p.getRelatedParamter())
                .collect(Collectors.toList());
    }
}
