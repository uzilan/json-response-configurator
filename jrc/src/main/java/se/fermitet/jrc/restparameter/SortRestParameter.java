package se.fermitet.jrc.restparameter;

public class SortRestParameter {
	private String sortByName;
	private SortRestParameterKeyword sortByKeyword;
	private String sortCollection;
	private boolean ascending;
	private SortRestParameter relatedParameter;

	public enum SortRestParameterKeyword {
		MAPKEY;

		static SortRestParameterKeyword fromString(String sortByProperty) {
			if (sortByProperty.toUpperCase().equals("MAPKEY"))
				return SortRestParameterKeyword.MAPKEY;
			else
				throw new IllegalArgumentException("Cannot create SortRestParameterKeyword from string " + sortByProperty);
		}
	}

	public SortRestParameter(String sortByProperty) {
		this(null, sortByProperty);
	}

	public SortRestParameter(String sortCollection, String sortByProperty) {
		super();

		this.ascending = !sortByProperty.startsWith("-");
		String sortByPropertyWithoutMinus = this.ascending ? sortByProperty : sortByProperty.substring(1, sortByProperty.length());

		try {
			this.sortByKeyword = SortRestParameterKeyword.fromString(sortByPropertyWithoutMinus);
		} catch (IllegalArgumentException e) {
			this.sortByName = sortByPropertyWithoutMinus;
		}

		initCollection(sortCollection, sortByProperty);
	}

	private void initCollection(String sortCollection, String sortByProperty) {
		this.sortCollection = sortCollection;

		if (this.getSortCollection() == null || this.getSortCollection().isEmpty()) return;

		String newCollection = null;
		if (this.getSortCollection().contains(".")) {
			int idx = this.getSortCollection().indexOf(".");
			newCollection = this.getSortCollection().substring(idx + 1);
		}

		this.relatedParameter = new SortRestParameter(newCollection, sortByProperty);
	}

	public String getSortByName() {
		return this.sortByName;
	}

	public SortRestParameterKeyword getSortByKeyword() {
		return this.sortByKeyword;
	}

	public boolean isAscending() {
		return this.ascending;
	}

	public boolean isDescending() {
		return !this.isAscending();
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();

		buf.append("SortRestParameter -- ");
		if (this.getSortByName() != null) {
			buf.append("SortByName: ");
			buf.append(getSortByName());
		} else {
			buf.append("SortByKeyword: ");
			buf.append(getSortByKeyword());
		}

		buf.append("    ");

		if (isAscending())
			buf.append("ASCENDING");
		else
			buf.append("DESCENDING");

		if (this.getSortCollection() != null) {
			buf.append("    Collection: ");
			buf.append(getSortCollection());
		}

		return buf.toString();
	}

	public String getSortCollection() {
		return this.sortCollection;
	}

	public SortRestParameter getRelatedParamter() {
		return this.relatedParameter;
	}


}
