package se.fermitet.jrc.restparameter;

import java.util.Collection;
import java.util.List;

import javax.enterprise.context.RequestScoped;

@RequestScoped
public class RestParameterData {
	private Collection<String> attributeNames;
	private Collection<String> relationNames;
	private List<SortRestParameter> sortParameters;

	public Collection<String> getAttributeNames() {
		return attributeNames;
	}

	public void setAttributeNames(Collection<String> attributeNames) {
		this.attributeNames = attributeNames;
	}

	public Collection<String> getRelationNames() {
		return relationNames;
	}

	public void setRelationNames(Collection<String> relationNames) {
		this.relationNames = relationNames;
	}

	public List<SortRestParameter> getSortParameters() {
		return sortParameters;
	}

	public void setSortParameters(List<SortRestParameter> sortParameters) {
		this.sortParameters = sortParameters;
	}


}
