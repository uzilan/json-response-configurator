package se.fermitet.jrc.restparameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

@Provider
public class RestParameterFilter implements ContainerRequestFilter {
	@Inject
	RestParameterData restParameterData;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		List<String> attValues = new ArrayList<String>();
		List<String> relValues = new ArrayList<String>();
		List<SortRestParameter> sortParameters = new ArrayList<SortRestParameter>();

		MultivaluedMap<String, String> parameters = requestContext.getUriInfo().getQueryParameters();

		for (String key : parameters.keySet()) {
			Collection<String> parametersForThisKey = parameters.get(key);

			if (key.equals("att"))
				addToStringCollection(attValues, parametersForThisKey);
			else if (key.equals("rel"))
				addToStringCollection(relValues, parametersForThisKey);
			else if (key.startsWith("sort"))
				handleSortParameters(key, sortParameters, parametersForThisKey);
		}

		restParameterData.setAttributeNames(attValues);
		restParameterData.setRelationNames(relValues);
		restParameterData.setSortParameters(sortParameters);
	}

	private void addToStringCollection(Collection<String> collectionToAddTo, Collection<String> parametersForThisKey) {
		for (String value : parametersForThisKey) {
			collectionToAddTo.addAll(splitStringOnComma(value));
		}
	}

	private void handleSortParameters(String key, List<SortRestParameter> sortParameters, Collection<String> parametersForThisKey) {
		String collectionPart = null;
		if (key.length() > 5)
			collectionPart = key.substring(5, key.length());

		for (String listOfNames : parametersForThisKey) {
			for (String name : splitStringOnComma(listOfNames)) {
				sortParameters.add(new SortRestParameter(collectionPart, name));
			}
		}
	}


	private Collection<String> splitStringOnComma(String value) {
		return Arrays.asList(value.split(","));
	}

}
