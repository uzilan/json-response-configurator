package se.fermitet.jrc.property;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.stream.Collectors;

import se.fermitet.jrc.annotation.Payload;

public class PayloadProperty extends Property {

	public PayloadProperty(String name, Class<?> clz) {
		super(name, clz);

		if (! super.getProperty().isAnnotationPresent(Payload.class))
			throw new IllegalArgumentException("The property with name: " + super.getName() + " in class: " + clz.getName() + " is not a payload");
	}

	public static PayloadProperty getPayloadProperty(Class<?> clz) {
		Collection<PayloadProperty> all = getAllPayloadProperties(clz);

		if (all.isEmpty())
			return null;
		else if (all.size() == 1) 
			return all.iterator().next();
		else
			throw new IllegalArgumentException("Only 0 or 1 payloads are allowed.  The class " + clz.getName() + " has " + all.size());
	}

	private static Collection<PayloadProperty> getAllPayloadProperties(Class<?> clz) {
		Collection<PayloadProperty> result = getAllFieldsAndMethodsIncludingSuperclass(clz).stream()
				.filter(a -> a.isAnnotationPresent(Payload.class))
				.map(a ->
				{
					String name = getPropertyNameFromFieldOrMethod(a);
					PayloadProperty prop = new PayloadProperty(name, clz);
					
					if (a instanceof Method)
						prop.setGetter((Method) a);

					return prop;
				}).collect(Collectors.toList());

		return result;
	}

	public static boolean hasDefinedPayload(Class<?> clz) {
		return (getPayloadProperty(clz) != null);
	}


}
