package se.fermitet.jrc.property;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import se.fermitet.jrc.annotation.Attribute;

public abstract class Property {
	private String name;
	private AccessibleObject property;
	private Method getter;
	private Method setter;
	private Class<?> clz;

	public Property(String name, Class<?> clz) {
		super();
		this.name = name;
		this.clz = clz;

		this.ensureProperty();
	}

	public String getName() {
		return this.name;
	}

	void setGetter(Method getter) {
		this.getter = getter;
	}

	public Object getValue(Object objectThatHasProperty) {
		ensureGetter();
		try {
			return this.getter.invoke(objectThatHasProperty);
		} catch (Exception e) {
			// Try to make it accessible to ensure that tests run
			this.getter.setAccessible(true);
			try {
				return this.getter.invoke(objectThatHasProperty);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
				throw new IllegalArgumentException("Error in getValue", e);
			}
		}			
	}

	public void setValue(Object newValue, Object objectThatHasProperty) {
		ensureSetter();
		try {
			this.setter.invoke(objectThatHasProperty, newValue);
		} catch (Exception e) {
			// Try to make it accessible to ensure that tests run
			this.setter.setAccessible(true);
			try {
				this.setter.invoke(objectThatHasProperty, newValue);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
				throw new IllegalArgumentException("Error in setValue", e);
			}
		}			
	}

	public static Collection<Property> getDefaultProperties(Class<?> clz) {
		Collection<Property> result = getAllFieldsAndMethodsIncludingSuperclass(clz).stream()
				.filter(a -> a.isAnnotationPresent(Attribute.class))
				.map(a ->
				{
					String name = getPropertyNameFromFieldOrMethod(a);
					AttributeProperty prop = new AttributeProperty(name, clz);
					
					if (a instanceof Method)
						prop.setGetter((Method) a);

					return prop;
				}).collect(Collectors.toList());

		return result;
	}

	protected static String getPropertyNameFromFieldOrMethod(AccessibleObject a) {
		String name = null;
		if (a instanceof Field) {
			Field f = (Field) a;
			name = f.getName();
		} else {
			Method m = (Method) a;

			name = m.getName();
			if (name.startsWith("get"))
				name = name.substring(3, name.length());
			else if (name.startsWith("is"))
				name = name.substring(2, name.length());
			else if (name.startsWith("has"))
				name = name.substring(3, name.length());

			name = name.substring(0, 1).toLowerCase() + name.substring(1, name.length());
		}
		return name;
	}

	public static Property getByName(String name, Class<?> clz) {
		try {
			if (isNameCompound(name))
				return new CompoundProperty(name, clz);
			else
				return new AttributeProperty(name, clz);
		} catch (IllegalArgumentException e) {
			return new RelationProperty(name, clz);
		}
	}

	public static boolean isNameCompound(String name) {
		return name.contains(".");
	}

	protected AccessibleObject getProperty() {
		return this.property;
	}

	protected Class<?> getClz() {
		return clz;
	}

	private void ensureProperty() {
		if (this.property != null) return;
		if (isNameCompound(this.getName())) return;

		this.property = getDeclaredFieldIncludingSuperclassesOrNull();

		if (this.property == null) {
			ensureGetter();
			this.property = this.getter;
		}

		if (this.property == null) {
			throw new IllegalArgumentException("There is no property with name: " + getName() + " in class: " + this.clz.getName());
		}
	}

	private void ensureGetter() {
		if (this.getter != null) return;

		String propertyNameWithFirstLetterInUpper = getName().substring(0,1).toUpperCase() + getName().substring(1, getName().length());

		this.getter = getDeclaredMethodIncludingSuperclassesOrNull("get" + propertyNameWithFirstLetterInUpper);
		if (this.getter == null)
			this.getter = getDeclaredMethodIncludingSuperclassesOrNull("is" + propertyNameWithFirstLetterInUpper);
		if (this.getter == null)
			this.getter = getDeclaredMethodIncludingSuperclassesOrNull("has" + propertyNameWithFirstLetterInUpper);

		if (this.getter == null) {
			throw new IllegalArgumentException("No getter found for property with name: " + getName() + " in class: " + clz.getName());
		}
	}

	private void ensureSetter() {
		ensureGetter();
		if (this.setter != null) return;

		String propertyNameWithFirstLetterInUpper = getName().substring(0,1).toUpperCase() + getName().substring(1, getName().length());
		this.setter = getDeclaredMethodIncludingSuperclassesOrNull("set" + propertyNameWithFirstLetterInUpper, this.getter.getReturnType());

		if (this.setter == null) {
			throw new IllegalArgumentException("No setter found for property with name: " + getName() + " in class: " + clz.getName());
		}

	}

	private Field getDeclaredFieldIncludingSuperclassesOrNull() {
		Class<?> workingClass = clz;

		Field ret = null;

		while(ret == null && workingClass != null) {
			try {
				ret = workingClass.getDeclaredField(getName());
			} catch (NoSuchFieldException e) {
				// Do nothing
			}
			workingClass = workingClass.getSuperclass();
		}

		return ret;
	}

	private Method getDeclaredMethodIncludingSuperclassesOrNull(String methodName, Class<?>... parameterTypes) {
		Class<?> workingClass = clz;

		Method ret = null;

		while(ret == null && workingClass != null) {
			try {
				ret = workingClass.getDeclaredMethod(methodName, parameterTypes);
			} catch (NoSuchMethodException e) {
				// Do nothing
			}
			workingClass = workingClass.getSuperclass();
		}

		return ret;
	}

	protected static Collection<AccessibleObject> getAllFieldsAndMethodsIncludingSuperclass(Class<?> clz) {
		Collection<AccessibleObject> ret = new ArrayList<AccessibleObject>();

		Class<?> workingClass = clz;

		while(workingClass != null) {
			ret.addAll(Arrays.asList(workingClass.getDeclaredFields()));
			ret.addAll(Arrays.asList(workingClass.getDeclaredMethods()));
			workingClass = workingClass.getSuperclass();
		}

		return ret;
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();

		buf.append(this.getClass().getSimpleName() + "   ");
		buf.append(this.getName() + " on ");
		buf.append(clz.getName());

		return buf.toString();
	}

}
