package se.fermitet.jrc.property;

import se.fermitet.jrc.annotation.Relation;

public class RelationProperty extends Property {
	public RelationProperty(String name,  Class<?> clz) {
		super(name, clz);

		if (! super.getProperty().isAnnotationPresent(Relation.class))
			throw new IllegalArgumentException("The property with name: " + super.getName() + " in class: " + clz.getName() + " is not a relation");
	}
}